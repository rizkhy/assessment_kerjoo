<?php

namespace App\Http\Controllers\API\AnnualLeaves;

use App\Models\AnnualLeaves;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\AnnualLeavesPostRequest;

class AnnualLeavesController extends Controller
{
    public function storeAnnualLeave(AnnualLeavesPostRequest $request)
    {
        DB::beginTransaction();

        $annualLeave = AnnualLeaves::create([
            'user_id' => $request->user_id,
            'reason' => $request->reason,
            'status' => $request->status,
        ]);

        foreach ($request->dates as $date) {
            $annualLeave->dates()->create([
                'date' => $date,
            ]);
        }

        DB::commit();

        return response()->json([
            'message' => 'Annual leave created successfully',
            'data' => $annualLeave
        ], 201);
    }

    public function detailAnnualLeave($id)
    {
        $annualLeave = AnnualLeaves::with('dates')->find($id);

        return response()->json([
            'message' => 'Annual leave created successfully',
            'data' => $annualLeave
        ], 201);
    }

    public function getAnnualLeave()
    {
        $annualLeave = AnnualLeaves::with('dates')->get();

        return response()->json([
            'message' => 'Annual leave created successfully',
            'data' => $annualLeave
        ], 201);
    }
}
