<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TestGridController extends Controller
{
    public function testGrid()
    {
        return view('web.test-grid');
    }
}
