<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TestFlexController extends Controller
{
    public function testFlex()
    {
        return view('web.test-flex');
    }
}
