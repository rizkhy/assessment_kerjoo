<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnnualLeaves extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'reason',
        'status',
    ];

    public function dates()
    {
        return $this->hasMany(AnnualLeaveDates::class, 'annual_leave_id');
    }
}
