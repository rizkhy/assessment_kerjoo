<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnnualLeaveDates extends Model
{
    use HasFactory;

    protected $fillable = [
        'annual_leave_id',
        'date',
    ];

    public function annualLeave()
    {
        return $this->belongsTo(AnnualLeaves::class, 'annual_leave_id');
    }
}
