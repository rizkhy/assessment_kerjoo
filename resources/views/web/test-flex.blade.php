<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Test Flex</title>
</head>

<body>

    <div class="container">
        <div class="d-xl-flex d-md-inline-flex d-sm-inline-flex border border-1 w-100">
            <div class="p-2 border-1">
                <button type="button" class="btn btn-primary">Tambah</button>
                <button type="button" class="btn btn-success">Import</button>
                <button type="button" class="btn btn-warning">Export</button>
            </div>
            <div class="p-2 flex-fill border-1 w-32">
                <input type="text" name="search" class="form-control" placeholder="Search...">
            </div>
            <div class="p-2 flex-fill border-1 w-32">
                <select name="year" class="form-select">
                    <option value="2020">2020</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                </select>
            </div>
        </div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
</body>

</html>
