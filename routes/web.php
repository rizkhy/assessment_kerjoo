<?php

use App\Http\Controllers\Web\TestFlexController;
use App\Http\Controllers\Web\TestGridController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/test-grid', [TestGridController::class, 'testGrid'])->name('test-grid');
Route::get('/test-flex', [TestFlexController::class, 'testFlex'])->name('test-flex');
