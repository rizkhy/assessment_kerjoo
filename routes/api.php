<?php

use App\Http\Controllers\API\AnnualLeaves\AnnualLeavesController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('/annual-leaves', [AnnualLeavesController::class, 'storeAnnualLeave']);

Route::get('/annual-leaves/{id}', [AnnualLeavesController::class, 'detailAnnualLeave']);

Route::get('/annual-leaves', [AnnualLeavesController::class, 'getAnnualLeave']);
